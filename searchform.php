<form method="get" id="searchform" class="search-form" action="<?php echo esc_url( home_url( '/' ) ); ?>">
	<div>
		<input class="search-field" type="text" value="" name="s" id="s" placeholder="<?php esc_attr_e( "Search here", 'hotel-california' ); ?>">
		<input type="submit" class="search-submit" value="Search">
	</div>
</form>