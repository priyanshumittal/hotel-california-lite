<?php 
/**
 * The main template file
 */
get_header();
hotel_california_breadcrumbs(); ?>
<!-- Blog & Sidebar Section -->
<!-- 404 Error Section -->
<section class="404-section">		
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="error_404 wow flipInX animated" data-wow-delay=".5s">
					<h4> <?php _e('Oops! Page not found','hotel-california'); ?></h4>
					<h1><?php _e('4','hotel-california'); ?><i class="fa fa-frown-o"></i><?php _e('4','hotel-california'); ?> </h1>
					<p><?php _e ('Page doesnt exist or some other error occured. Go to our','hotel-california'); ?> <a href="<?php echo esc_html(site_url());?>"><?php _e('home page','hotel-california'); ?></a></p>
				</div>
			</div>
		</div>
	</div>
</section>
<!-- /404 Error Section -->
<!-- /Blog & Sidebar Section -->
<?php get_footer(); ?>