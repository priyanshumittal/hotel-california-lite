<?php
function custom_light() {

	$link_color = '#e32235';

	list($r, $g, $b) = sscanf($link_color, "#%02x%02x%02x");
	$r = $r - 50;
	$g = $g - 25;
	$b = $b - 40;
	
	if ( $link_color != '#ff0000' ) :
	?>
	<style type="text/css"> 
	/* custom light */
	.navbar-default .navbar-nav li.current-menu-item > a,
	.navbar-default .navbar-nav li.current-menu-item > a:hover,
	.navbar-default .navbar-nav li.current-menu-item > a:focus,
	.navbar-default .navbar-nav li.active > a,
	.navbar-default .navbar-nav li.active > a:hover 
	{
		background-color:<?php echo $link_color; ?>;
	}
	
	.navbar-default .navbar-nav li > a:hover,
	.navbar-default .navbar-nav li > a:focus,
	.navbar-default .navbar-nav li.menu-item-has-children > a:hover,
	.navbar-default .navbar-nav li.menu-item-has-children > a:focus,
	.navbar-default .navbar-nav li.dropdown:hover > a,
	.navbar-default .navbar-nav li.dropdown:focus > a
	{
		color:<?php echo $link_color; ?>;
	}
	
	.navbar-default .navbar-nav >  .current-menu-item > a:hover,
	.navbar-default .navbar-nav >  .current-menu-item > a:focus{ 
		-moz-box-shadow: inset 0 0 5px <?php echo $link_color; ?>;
		-webkit-box-shadow: inset 0 0 5px <?php echo $link_color; ?>;
		box-shadow: inset 0 0 5px <?php echo $link_color; ?>;
	 }

	.dropdown-menu { 
		border-top: 2px solid <?php echo $link_color; ?>;
		border-bottom: 2px solid <?php echo $link_color; ?>;
	}
	
	.comment-date, 
	.testmonial-area span , .about-btn a:hover, 
	/* .page-breadcrumb > li.active a, */
	.author-description:before, 
	.about-section h2 > span, 
	.link-content p,
	.comment-date, 
	.link-content p > a, 
	.footer-copyright p a:hover, 
	.edit-link a, 
	.menu-long-menu-container,
	.about-btn a:hover, 
	ul.post-content li:hover a, 
	ul.post-content > li > a:before, 
	.error_404 h1, .error_404 p > a, 
	a, a:hover, a:focus, .portfolio-img i
	{ 
		color: <?php echo $link_color; ?>;
	}
	
	.format-status-btn-sm,  
	.format-video-btn-sm, 
	.home-room-btn 
	{
		background-color: <?php echo $link_color; ?>;
		color: #ffffff;
		box-shadow: 0 3px 0 0 #9a0e1c;
	}
	
	.sm-callout 
	{ 
		border-top: 2px solid <?php echo $link_color; ?>;
	}
	
	.footer-section 
	{ 
		border-top: 3px solid <?php echo $link_color; ?>;
		border-bottom: 3px solid <?php echo $link_color; ?>;
	}
	
	.format-status-btn-sm, 
	.format-video-btn-sm,  
	.sm-callout-btn a, 
	.reply a, 
	.cont-btn a, 
	.cont-btn  input[type="submit"]:before, 
	.hc_scrollup, 
	.cont-btn  input[type="submit"]:hover, 
	input[type="submit"]::before, 
	.cont-btn  input[type="submit"]::before, 
	.reply a,  
	.cont-btn a, 
	.sm-callout-btn a, 
	.team-image .team-showcase-icons a:hover, 
	.title-separator span, 
	.scroll-btn li:hover, 
	
	.slide-btn-area-sm a.slide-btn-sm::before, 
	.home-room-btn a:hover, 
	.cont-btn  input[type="submit"], 
	.more-link, 
	.top-header-detail .section-title span, 
	.footer-sidebar .section-title span,  
	.separator span, 
	
	.widget #wp-calendar caption, 	
	.widget #wp-calendar tbody a, 
	.widget #wp-calendar tbody a:hover, .widget #wp-calendar tbody a:focus, 

	.sidebar .widget form.search-form input.search-submit, 
	.top-header-detail .widget form.search-form input.search-submit, 
	.footer-sidebar .widget form.search-form input.search-submit, 
	.portfolio-section .widget form.search-form input.search-submit, 
	.testimonial-section .widget form.search-form input.search-submit, 
	.amenities-section .widget form.search-form input.search-submit, 
	.callout-section .widget form.search-form input.search-submit, 
	.team-section .widget form.search-form input.search-submit, 
	.widget-section .widget form.search-form input.search-submit, 
	
	input[type="submit"],
	.page-links a,
		
	.widget .tagcloud a:hover,
	mark, ins, 
	.page-title-section
	{ 
		background-color: <?php echo $link_color; ?>;
	}

	.format-status-btn-sm, 
	.format-video-btn-sm, 
	.home-room-btn a, 
	.sm-callout-btn a, 
	.reply a, 
	.cont-btn a, 
	.cont-btn input[type="submit"],  
	.sm-callout-btn a, 
	.cont-btn  input[type="submit"]:hover, 
	.cont-btn input[type="submit"], 
	a.more-link, 
	
	input[type="submit"],
	.page-links a,
	 
	.widget form.search-form input.search-submit
	{  
		box-shadow: 0 3px 0 0 rgb(<?php echo $r; ?>,<?php echo $g; ?>,<?php echo $b;?>);
	}
	
	/* 404 */
	.error_404 h1, 
	.error_404 p > a,
	.portfolio-content-area:hover .portfolio-content h3 > a, 
	.portfolio-content-area:hover h3 > a, 
	.widget-section .post-content-area:hover h3.entry-title a, 
	
	.widget li a::before, 
	.widget li a:hover, 
	
	.widget #wp-calendar a:hover, 
	.widget #wp-calendar #next a:hover, 
	.widget #wp-calendar #prev a:hover, 
	
	.comment-form-section .blog-post-info-detail a, 
	.comment-form-section .blog-post-info-detail a:hover, 
	.comment-form-section .blog-post-info-detail a:focus,  
	.blog-section .post-content-area .entry-content a[target=_blank], 
	.blog-section .post-content-area .page-links a, 
	.blog-section .post-content-area .entry-meta a:hover, 
	.blog-section .post-content-area:hover .entry-header h2.entry-title > a, 
	.blog-section .post-content-area .entry-content a	
	{ 
		color: <?php echo $link_color; ?>;
	}
	
	.sidebar .section-title,  
	blockquote
	{
		border-left: 5px solid <?php echo $link_color; ?>;
	}
	
	.sidebar .widget .tagcloud a:hover, 
	.top-header-detail .widget .tagcloud a:hover, 
	.footer-sidebar .widget .tagcloud a:hover, 
	.portfolio-section .widget .tagcloud a:hover, 
	.testimonial-section .widget .tagcloud a:hover, 
	.amenities-section .widget .tagcloud a:hover, 
	.callout-section .widget .tagcloud a:hover, 
	.team-section .widget .tagcloud a:hover, 
	.widget-section .widget .tagcloud a:hover
	{
		border: 1px solid <?php echo $link_color; ?>;
	}
	
	</style>
<?php
	endif;
}