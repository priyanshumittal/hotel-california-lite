<?php
// Global variables define
define('WEBRITI_TEMPLATE_DIR_URI',get_template_directory_uri());	
define('WEBRITI_TEMPLATE_DIR',get_template_directory());
define('WEBRITI_THEME_FUNCTIONS_PATH',WEBRITI_TEMPLATE_DIR.'/functions');


// Theme functions file including
require( WEBRITI_THEME_FUNCTIONS_PATH . '/scripts/script.php');
require( WEBRITI_THEME_FUNCTIONS_PATH . '/menu/default_menu_walker.php');
require( WEBRITI_THEME_FUNCTIONS_PATH . '/menu/webriti_nav_walker.php');
require( WEBRITI_THEME_FUNCTIONS_PATH .'/font/font.php');
require( WEBRITI_THEME_FUNCTIONS_PATH . '/breadcrumbs/breadcrumbs.php');
require( WEBRITI_THEME_FUNCTIONS_PATH . '/template-tags.php');
require( WEBRITI_TEMPLATE_DIR . '/css/custom_light.php');
require( WEBRITI_THEME_FUNCTIONS_PATH . '/widgets/sidebars.php');
require( WEBRITI_THEME_FUNCTIONS_PATH . '/widgets/wdl_feature_page_widget.php');

// Adding customizer files
require( WEBRITI_THEME_FUNCTIONS_PATH . '/customizer/customizer_header_image.php');
require( WEBRITI_THEME_FUNCTIONS_PATH . '/customizer/customizer_slider.php');
require( WEBRITI_THEME_FUNCTIONS_PATH . '/customizer/customizer_general_settings.php');
require( WEBRITI_THEME_FUNCTIONS_PATH . '/customizer/customizer_home_page.php');

// Hotel California theme title
if( !function_exists( 'hotel_california_head_title' ) ) 
{
	function hotel_california_head_title( $title , $sep ) {
		global $paged, $page;
		
		if ( is_feed() )
				return $title;
			
		// Add the site name
		$title .= get_bloginfo( 'name' );
		
		// Add the site description for the home / front page
		$site_description = get_bloginfo( 'description' );
		if ( $site_description && ( is_home() || is_front_page() ) )
				$title = "$title $sep $site_description";
			
		// Add a page number if necessary.
		if ( ( $paged >= 2 || $page >= 2 ) && ! is_404() )
				$title = "$title $sep " . sprintf( __( 'Page %s', 'hotel-california' ), max( $paged, $page ) );
			
		return $title;
	}
}
add_filter( 'wp_title', 'hotel_california_head_title', 10, 2);

// Theme setup
if( !function_exists( 'hotel_california_theme_setup' ) )
{
	function hotel_california_theme_setup(){
		
		global $content_width;
		if ( ! isset( $content_width ) ) $content_width = 600;
		
		// Load text domain for translation-ready
		load_theme_textdomain( 'hotel-california', WEBRITI_THEME_FUNCTIONS_PATH . '/lang' );
		
		// supports featured image
		add_theme_support( 'post-thumbnails' );
		
		// main menu navigation bar
		register_nav_menu( 'primary', __( 'Primary Menu', 'hotel-california' ) );
		
		// Let WordPress manage the document title.
		add_theme_support( 'title-tag' );
		
		// automatic feeds
		add_theme_support( 'automatic-feed-links');
		
		// shortcode support
		add_filter('widget_text','do_shortcode');
		
		// post format support
		add_theme_support( 'post-formats', array('aside', 'quote', 'status', 'video','gallery','audio') );
		
		
		//Custom logo
		
		add_theme_support( 'custom-logo', array(
			'height'      => 240,
			'width'       => 240,
			'flex-height' => true,
			'header-text' => array( 'site-title', 'site-description' ),
			
		) );
		
		add_theme_support( 'custom-header', apply_filters( 'hotel_california_custom_header_args', array(
				'default-text-color'     => '333',
				'width'                  => 1600,
				'height'                 => 200,
				'flex-height'            => true,
				'wp-head-callback'       => 'hotel_california_header_style',
			) ) );
	}
}
add_action( 'after_setup_theme', 'hotel_california_theme_setup' );

function hotel_california_registers() {

	wp_enqueue_script( 'hotel_california_customizer_script', get_template_directory_uri() . '/js/hotel_customizer.js', array("jquery"), '20120206', true  );
}
add_action( 'customize_controls_enqueue_scripts', 'hotel_california_registers' );