<?php	
add_action( 'widgets_init', 'hotel_california_widgets_init');
function hotel_california_widgets_init() {
	
	//Room Layout
	$room_layout =get_theme_mod('room_column_layout',2);
	$room_layout = 12 / $room_layout;
	
	/*sidebar*/
	
	register_sidebar( array(
		'name' => __( 'Sidebar Widget Area', 'hotel-california' ),
		'id' => 'sidebar_primary',
		'description' => __( 'The right sidebar widget area', 'hotel-california' ),
		'before_widget' => '<div id="%1$s" class="widget %2$s wow fadeInDown animated" data-wow-delay="0.4s">',
		'after_widget' => '</div>',
		'before_title' => '<div class="section-title wow fadeInDown animated" data-wow-delay="0.4s"><h3 class="widget-title">',
		'after_title' => '</h3></div>',
	) );
		
	register_sidebar( array(
		'name' => __( 'Top Header Sidebar left', 'hotel-california' ),
		'id' => 'home-header-sidebar_left',
		'description' => __( 'Top Header Left Sidebar Area', 'hotel-california' ),
		'before_widget' => '<div id="%1$s" class="widget %2$s wow fadeInDown animated" data-wow-delay="0.4s">',
		'after_widget' => '</div>',
		'before_title' => '<div class="section-title"><h3 class="widget-title">',
		'after_title' => '</h3><span></span></div>',
	) );
	
	register_sidebar( array(
		'name' => __( 'Top Header Sidebar Right', 'hotel-california' ),
		'id' => 'home-header-sidebar_right',
		'description' => __( 'Top Header Right Sidebar Area', 'hotel-california' ),
		'before_widget' => '<div id="%1$s" class="widget %2$s wow fadeInDown animated" data-wow-delay="0.4s">',
		'after_widget' => '</div>',
		'before_title' => '<div class="section-title"><h3 class="widget-title">',
		'after_title' => '</h3><span></span></div>',
	) );
	
	register_sidebar( array(
		'name' => __( 'Footer Widget Area Left', 'hotel-california' ),
		'id' => 'footer_widget_area_left',
		'description' => __( 'footer widget left area', 'hotel-california' ),
		'before_widget' => '<div id="%1$s" class="widget %2$s wow fadeInDown animated" data-wow-delay="0.4s">',
		'after_widget' => '</div>',
		'before_title' => '<div class="section-title"><h3 class="widget-title">',
		'after_title' => '</h3><span></span></div>',
	) );
	
	register_sidebar( array(
		'name' => __( 'Footer Widget Area Center', 'hotel-california' ),
		'id' => 'footer_widget_area_center',
		'description' => __( 'footer widget center area', 'hotel-california' ),
		'before_widget' => '<div id="%1$s" class="widget %2$s wow fadeInDown animated" data-wow-delay="0.4s">',
		'after_widget' => '</div>',
		'before_title' => '<div class="section-title"><h3 class="widget-title">',
		'after_title' => '</h3><span></span></div>',
	) );
	
	register_sidebar( array(
		'name' => __( 'Footer Widget Area Right', 'hotel-california' ),
		'id' => 'footer_widget_area_right',
		'description' => __( 'footer widget right area', 'hotel-california' ),
		'before_widget' => '<div id="%1$s" class="widget %2$s wow fadeInDown animated" data-wow-delay="0.4s">',
		'after_widget' => '</div>',
		'before_title' => '<div class="section-title"><h3 class="widget-title">',
		'after_title' => '</h3><span></span></div>',
	) );
	
	// business content top sidebar
	register_sidebar( array(
		'name' => __( 'Home Page Rooms Section Sidebar', 'hotel-california' ),
		'id' => 'wdl_top_sidebar',
		'description' => __( 'This section for suitable widgets like WDL: Rooms widget.', 'hotel-california' ),
		'before_widget' => '<div id="%1$s" class="widget col-md-3 %2$s">',
		'after_widget' => '</div>',
		'before_title' => '<h1 class="widget-title wow fadeInUp animated" data-wow-duration="500ms" data-wow-delay="0ms">',
		'after_title' => '</h1>'
	));
}	                     
?>