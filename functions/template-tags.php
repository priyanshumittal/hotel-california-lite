<?php
if ( ! function_exists( 'hotel_california_blog_meta_content' ) ) :
function hotel_california_blog_meta_content()
{ 
	$blog_meta_section_enable = get_theme_mod('blog_meta_section_enable',false);
	
	if( $blog_meta_section_enable == false ) { ?>
	<div class="entry-meta">
		<span class="author"><a href="<?php echo get_author_posts_url( get_the_author_meta( 'ID' ) );?>"><?php _e('Posted by: ','hotel-california'); echo get_the_author();?></a></span>
		<span class="entry-date"><a href="<?php echo get_month_link(get_post_time('Y'),get_post_time('m')); ?>"><time datetime="" class="entry-date"><?php _e('Posted on: ','hotel-california'); echo get_the_date('M j, Y'); ?></time></a></span>
		<?php the_tags( '<span class="tag-links">', ', ', '</span>' ); ?>				
	</div>
<?php } 
} 
endif;

// avator class
function hotel_california_gravatar_class($class) {
    $class = str_replace("class='avatar", "class='img-responsive img-circle", $class);
    return $class;
}
add_filter('get_avatar','hotel_california_gravatar_class');


// webriti author meta
function hotel_california_author_meta()
{ ?>
<article class="blog-author wow fadeInDown animated" data-wow-delay="0.4s">
	<div class="media">
		<div class="pull-left">
			<?php echo get_avatar( get_the_author_meta('ID'), 200); ?>
		</div>
		<div class="media-body">
			<h6><?php the_author_link(); ?></h6>
			<p><?php the_author_meta( 'description' ); ?></p>
			<ul class="blog-author-social">
			    <?php $facebook_profile = get_the_author_meta( 'facebook_profile' ); if ( $facebook_profile && $facebook_profile != '' ): ?>
				<li class="facebook"><a href="<?php echo esc_url($facebook_profile); ?>"><i class="fa fa-facebook"></i></a></li>
				<?php endif; ?>
				<?php $linkedin_profile = get_the_author_meta( 'linkedin_profile' ); if ( $linkedin_profile && $linkedin_profile != '' ): ?>
				<li class="linkedin"><a href="<?php echo esc_url($linkedin_profile); ?>"><i class="fa fa-linkedin"></i></a></li>
				<?php endif; ?>
				<?php $twitter_profile = get_the_author_meta( 'twitter_profile' ); if ( $twitter_profile && $twitter_profile != '' ): ?>
				<li class="twitter"><a href="<?php echo esc_url($twitter_profile); ?>"><i class="fa fa-twitter"></i></a></li>
				<?php endif; ?>
				<?php $google_profile = get_the_author_meta( 'google_profile' ); if ( $google_profile && $google_profile != '' ): ?>
				<li class="googleplus"><a href="<?php echo esc_url($google_profile); ?>"><i class="fa fa-google-plus"></i></a></li>
				<?php endif; ?>
		   </ul>
		</div>
	</div>	
</article>
<?php }

// author profile data
function hotel_california_author_social_icons( $contactmethods ) {
		$contactmethods['facebook_profile'] = __('Facebook Profile URL','hotel-california');
		$contactmethods['twitter_profile'] = __('Twitter Profile URL','hotel-california');
		$contactmethods['linkedin_profile'] = __('Linkedin Profile URL','hotel-california');
		$contactmethods['google_profile'] = __('Google Profile URL','hotel-california');
		return $contactmethods;
	}
add_filter( 'user_contactmethods', 'hotel_california_author_social_icons', 10, 1);

// blogs,pages and archive page title
function hotel_california_archive_page_title(){
	if( is_archive() )
	{
		$archive_text = get_theme_mod('archive_prefix','Archives for');
		
		echo '<div class="page-title wow bounceInLeft animated" ata-wow-delay="0.4s"><h1>';
		
		if ( is_day() ) :
		
		  printf( __( '%1$s %2$s', 'hotel-california' ), $archive_text, get_the_date() );
		  
        elseif ( is_month() ) :
		
		  printf( __( '%1$s %2$s', 'hotel-california' ), $archive_text, get_the_date( 'F Y' ) );
		  
        elseif ( is_year() ) :
		
		  printf( __( '%1$s %2$s', 'hotel-california' ), $archive_text, get_the_date( 'Y' ) );
		  
        elseif( is_category() ):
		
			$category_text = get_theme_mod('category_prefix','Category Archives');
			
			printf( __( '%1$s %2$s', 'hotel-california' ), $category_text, single_cat_title( '', false ) );
			
		elseif( is_author() ):
			
			$author_text = get_theme_mod('author_prefix','All posts by');
		
			printf( __( '%1$s %2$s', 'hotel-california' ), $author_text, get_the_author() );
			
		elseif( is_tag() ):
			
			$tag_text = get_theme_mod('tag_prefix','Tag Archives');
			
			printf( __( '%1$s %2$s', 'hotel-california' ), $tag_text, single_tag_title( '', false ) );
			
        endif;

		echo '</h1></div>';
	}
	elseif( is_search() )
	{
		$search_text = get_theme_mod('search_prefix','Search Results for');
		
		echo '<div class="page-title wow bounceInLeft animated" ata-wow-delay="0.4s"><h1>';
		
		printf( __( '%1$s %2$s', 'hotel-california' ), $search_text, get_search_query() );
		
		echo '</h1></div>';
	}
	elseif( is_404() )
	{
		$breadcrumbs_text = get_theme_mod('404_prefix','Error 404 : Page Not Found');
		
		echo '<div class="page-title wow bounceInLeft animated" ata-wow-delay="0.4s"><h1>';
		
		printf( __( '%1$s ', 'hotel-california' ) , $breadcrumbs_text );
		
		echo '</h1></div>';
	}
	else
	{
		echo '<div class="page-title wow bounceInLeft animated" ata-wow-delay="0.4s"><h1>'.get_the_title().'</h1></div>';
	}
}


function hotel_california_excerpt_more( $more ) {
	return '</div><div class="blog-btn"><a href="' . get_permalink() . '" class="home-blog-btn">'.__('Read More','hotel-california').'</a>';
}
add_filter( 'excerpt_more', 'hotel_california_excerpt_more' );

// hotel post navigation
function hotel_california_post_nav()
{
	global $post;
	echo '<div style="text-align:center;">';
	posts_nav_link( ' &#183; ', 'previous page', 'next page' );
	echo '</div>';
}

// Custom header function
if ( ! function_exists( 'hotel_california_header_style' ) ) :

function hotel_california_header_style() {
	$text_color = get_header_textcolor();

	// If no custom color for text is set, let's bail.
	if ( display_header_text() && $text_color === get_theme_support( 'custom-header', 'default-text-color' ) )
		return;
	?>
	<style type="text/css" id="hotel-california-header-css">
		<?php
		// Has the text been hidden?
		if ( ! display_header_text() ) :
	?>
		.site-title,
		.site-description {
			clip: rect(1px 1px 1px 1px); /* IE7 */
			clip: rect(1px, 1px, 1px, 1px);
			position: absolute;
		}
	<?php
		// If the user has set a custom color for the text, use that.
		elseif ( $text_color != get_theme_support( 'custom-header', 'default-text-color' ) ) :
	?>
		.site-title a {
			color: #<?php echo esc_attr( $text_color ); ?>;
		}
	<?php endif; ?>
	</style>
	<?php
}
endif;
?>