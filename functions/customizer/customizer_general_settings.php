<?php 
function hotel_california_general_settings_customizer( $wp_customize ){

/* Home Page Panel */
	$wp_customize->add_panel( 'general_settings', array(
		'priority'       => 125,
		'capability'     => 'edit_theme_options',
		'title'      => __('General Settings', 'hotel-california'),
	) );
	
	
	
	/* custom css section */
		$wp_customize->add_section( 'custom_css_section' , array(
			'title'      => __('Custom CSS', 'hotel-california'),
			'panel'  => 'general_settings'
		) );
		
			// Custom css
			$wp_customize->add_setting( 'hotel_custom_css' , array(
			'sanitize_callback'    => 'wp_filter_nohtml_kses',
			'sanitize_js_callback' => 'wp_filter_nohtml_kses'
			) );
			$wp_customize->add_control('hotel_custom_css' , array(
			'label'          => __( 'Custom CSS', 'hotel-california' ),
			'section'        => 'custom_css_section',
			'type'           => 'textarea'
			) );
	
	/* footer copyright section */
	$wp_customize->add_section( 'hotel_footer_copyright' , array(
		'title'      => __('Footer Copyright', 'hotel-california'),
		'panel'  => 'general_settings',
		'priority'   => 6,
   	) );
	
	$wp_customize->add_setting(
		'footer_copyright_text',
		array(
			'default'           =>  '<p>Copyright @ 2015 Hotel California. All right reserved</p>',
			'capability'        =>  'edit_theme_options',
			'sanitize_callback' =>  'hotel_california_copyright_sanitize_text',
		)	
	);
	$wp_customize->add_control('footer_copyright_text', array(
			'label' => __('Footer Copyright text & Link','hotel-california'),
			'section' => 'hotel_footer_copyright',
			'type'    =>  'textarea'
	));	 // footer copyright
	
	function hotel_california_copyright_sanitize_text( $input ) 
	{
		return wp_kses_post( force_balance_tags( $input ) );
	}
}
add_action( 'customize_register', 'hotel_california_general_settings_customizer' );