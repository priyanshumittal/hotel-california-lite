<?php
function hotel_california_slider_customizer( $wp_customize ) {
	
	
// list control categories	
if ( ! class_exists( 'WP_Customize_Control' ) ) return NULL;

 class Category_Dropdown_Custom_Control1 extends WP_Customize_Control
 {
    private $cats = false;
	
    public function __construct($wp_customize, $id, $args = array(), $options = array())
    {
        $this->cats = get_categories($options);
        parent::__construct( $wp_customize, $id, $args );
    }

    public function render_content()
       {
            if(!empty($this->cats))
            {
                ?>
                    <label>
                      <span class="customize-control-title"><?php echo esc_html( $this->label ); ?></span>
                      <select multiple="multiple" <?php $this->link(); ?>>
                           <?php
                                foreach ( $this->cats as $cat )
                                {
                                    printf('<option value="%s" %s>%s</option>', $cat->term_id, selected($this->value(), $cat->term_id, false), $cat->name);
                                }
                           ?>
                      </select>
                    </label>
                <?php
            }
       }
 }	

	//slider Section 
	$wp_customize->add_panel( 'hotel_california_slider_setting', array(
		'priority'       => 400,
		'capability'     => 'edit_theme_options',
		'title'      => __('Slider Section', 'hotel-california'),
	) );
	
	
	/* slider settings */
	$wp_customize->add_section( 'slider_settings' , array(
		'title'      => __('Slider Settings', 'hotel-california'),
		'panel'  => 'hotel_california_slider_setting',
   	) );
	
	
	$wp_customize->add_setting(
			'slider_enabled',
			array(
				'default' => true,
				'capability'     => 'edit_theme_options',
				'sanitize_callback' => 'sanitize_text_field',
			)	
			);
			$wp_customize->add_control(
			'slider_enabled',
			array(
				'label' => __('Enable Home Slider','hotel-california'),
				'section' => 'slider_settings',
				'type' => 'checkbox',
				'description' => __('Enable slider on front page.','hotel-california'),
			));
	
	
	// add section to manage featured slider on category basis	
	$wp_customize->add_setting(
    'slider_select_category',
    array(
        'default' => 2,
		'capability'     => 'edit_theme_options',
		'sanitize_callback' => 'sanitize_text_field',
		''
		));	
	$wp_customize->add_control( new Category_Dropdown_Custom_Control1( $wp_customize, 'slider_select_category', array(
    'label'   => __('Select Category for Slider','hotel-california'),
    'section' => 'slider_settings',
    'settings'   =>  'slider_select_category',
	) ) );
	
	//Slider animation
	$wp_customize->add_setting(
    'slider_options',
    array(
        'default' => __('slide','hotel-california'),
		'sanitize_callback' => 'sanitize_text_field',
    ));

	$wp_customize->add_control('slider_options',
		array(
			'type' => 'select',
			'label' => __('Slider effect type','hotel-california'),
			'section' => 'slider_settings',
			 'choices' => array('slide'=>__('slide', 'hotel-california'), 'carousel-fade'=>__('Fade', 'hotel-california')),
		));
		
	//Slider Animation duration
	$wp_customize->add_setting(
    'slider_transition_delay',
    array(
        'default' => __('3000','hotel-california'),
		'sanitize_callback' => 'sanitize_text_field',
    ));
	$wp_customize->add_control(
    'slider_transition_delay',
    array(
        'type' => 'text',
        'label' => __('Slider Duration','hotel-california'),
        'section' => 'slider_settings',
		));	
	
	}
	add_action( 'customize_register', 'hotel_california_slider_customizer' );
	?>