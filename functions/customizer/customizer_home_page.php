<?php 

// Adding customizer room section settings

function hotel_california_room_section_customizer( $wp_customize ){

/* Room Section Panel */
	$wp_customize->add_panel( 'room_section', array(
		'priority'       => 450,
		'title'      => __('Room Sections', 'hotel-california'),
	) );
	
	
		$wp_customize->add_section('home_room_page_section',array(
		'title' => __('General Section Settings','hotel-california'),
		'panel' => 'room_section',
		'priority'       => 10,
		));
		
			
			// enable room section
			$wp_customize->add_setting('room_section_enable',array(
			'default' => false,
			'capability'     => 'edit_theme_options',
			'sanitize_callback' => 'sanitize_text_field',
			) );
			
			$wp_customize->add_control('room_section_enable',array(
			'label' => __('Hide Room Page Section','hotel-california'),
			'section' => 'home_room_page_section',
			'type' => 'checkbox',
			) );
			
			
			// Number of Column layout
			$wp_customize->add_setting('room_column_layout',array(
			'default' => 2,
			'sanitize_callback' => 'sanitize_text_field',
			) );

			$wp_customize->add_control('room_column_layout',array(
			'type' => 'select',
			'label' => __('Select Column Layout','hotel-california'),
			'section' => 'home_room_page_section',
			'choices' => array(1=>1,2=>2,3=>3,4=>4),
			) );
			
		$wp_customize->add_section('home_room_page_section_header',array(
			'title' => __('General Section Header','hotel-california'),
			'panel' => 'room_section',
			'priority'       => 10,
			));	
		
		    // room section title
			$wp_customize->add_setting( 'home_room_section_title',array(
			'capability'     => 'edit_theme_options',
			'sanitize_callback' => 'hotel_california_home_page_sanitize_text',
			));	
			$wp_customize->add_control( 'home_room_section_title',array(
			'label'   => __('Section Title','hotel-california'),
			'section' => 'home_room_page_section_header',
			'type' => 'text',
			));	
			
			//room section discription
			$wp_customize->add_setting( 'home_room_section_discription',array(
			'capability'     => 'edit_theme_options',
			'sanitize_callback' => 'hotel_california_home_page_sanitize_text',
			));	
			$wp_customize->add_control( 'home_room_section_discription',array(
			'label'   => __('Section Discription','hotel-california'),
			'section' => 'home_room_page_section_header',
			'type' => 'textarea',
			));		
			
			
			
	
}
add_action( 'customize_register', 'hotel_california_room_section_customizer' );
function hotel_california_home_page_sanitize_text( $input ) {

			return wp_kses_post( force_balance_tags( $input ) );

			}