<?php

// Webriti scripts
if( !function_exists('hotel_california_scripts_function'))
{
	function hotel_california_scripts_function(){
		// css
		wp_enqueue_style('hotel-style', get_stylesheet_uri() );
		wp_enqueue_style('bootstrap', WEBRITI_TEMPLATE_DIR_URI . '/css/bootstrap.css');
		wp_enqueue_style('default', WEBRITI_TEMPLATE_DIR_URI . '/css/default.css');
		wp_enqueue_style('hotel_california_theme_theme-menu_css', WEBRITI_TEMPLATE_DIR_URI . '/css/theme-menu.css');
		wp_enqueue_style('hotel_california_theme_animate.min_css', WEBRITI_TEMPLATE_DIR_URI . '/css/animate.min.css');
		wp_enqueue_style('hotel_california_theme_font-awesome.min_css', WEBRITI_TEMPLATE_DIR_URI . '/css/font-awesome/css/font-awesome.min.css');
		wp_enqueue_style('hotel_california_theme_media-responsive_css', WEBRITI_TEMPLATE_DIR_URI . '/css/media-responsive.css');
		wp_enqueue_style('hotel_california_theme_element_css', WEBRITI_TEMPLATE_DIR_URI . '/css/element.css');
		
		wp_enqueue_script( 'jquery' );
		
		// Menu & page scroll js
		wp_enqueue_script('hotel_california_menu_js', WEBRITI_TEMPLATE_DIR_URI . '/js/menu/menu.js');
		wp_enqueue_script('hotel_california_page-scroll_js', WEBRITI_TEMPLATE_DIR_URI . '/js/page-scroll.js');
		
		
		if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
		}
	}
}
add_action('wp_enqueue_scripts','hotel_california_scripts_function');

// webriti footer enquque scripts
if(!function_exists('hotel_california_footer_enquque_scripts'))
{
	function hotel_california_footer_enquque_scripts()
	{
		wp_enqueue_script('hotel_california_bootstrap_js', WEBRITI_TEMPLATE_DIR_URI . '/js/bootstrap.min.js');
		wp_enqueue_script('hotel_california_animate_js', WEBRITI_TEMPLATE_DIR_URI . '/js/animation/animate.js');
		wp_enqueue_script('hotel_california_wow_js', WEBRITI_TEMPLATE_DIR_URI . '/js/animation/wow.min.js');
	}
}
add_action('wp_footer','hotel_california_footer_enquque_scripts');

// custom js enquque scripts
if(!function_exists('hotel_california_head_scripts'))
{
	function hotel_california_head_scripts()
	{ ?>
	<script>
		function changeCSS(cssFile, cssLinkIndex) {
			var link_index = document.getElementById("webriti_theme_default_css-css");
			link_index.setAttribute("href", "<?php echo WEBRITI_TEMPLATE_DIR_URI; ?>/css/"+cssFile);
		}
	</script>
	<?php }
}
add_action('wp_head','hotel_california_head_scripts');

function hotel_california_enqueue_scripts(){
	wp_enqueue_style('drag-drop-css', WEBRITI_TEMPLATE_DIR_URI . '/css/drag-drop.css');
}
add_action( 'admin_enqueue_scripts', 'hotel_california_enqueue_scripts' );

// custom css
function hotel_california_custom_css()
{
?>
	<style>
	<?php echo wp_filter_nohtml_kses(get_theme_mod('hotel_custom_css')); ?>
	</style>
<?php }
add_action('wp_head','hotel_california_custom_css');

// footer custom script
function hotel_california_footer_custom_script()
{
	custom_light();
}
add_action('wp_footer','hotel_california_footer_custom_script');
?>