<?php
/**
 * The default template for displaying content
 */
?>
<article id="post-<?php the_ID(); ?>" <?php post_class( 'post-content-area wow fadeInDown animated' ); ?> data-wow-delay="0.4s">
			
		<?php if ( is_single() ) {
			echo '<div class="blog-featured-img">';
			the_post_thumbnail( '', array( 'class'=>'img-responsive','alt' => get_the_title() ) );
			echo '</div>';
		}else{
			echo '<div class="blog-featured-img"><a class="post-thumbnail" href="'.get_the_permalink().'">';
			the_post_thumbnail( '', array( 'class'=>'img-responsive','alt' => get_the_title() ) );
			echo '</a></div>';
		} ?>
		
		<div class="post-content">
		
			<div class="entry-header">
				<?php
					if ( is_single() ) :
						the_title( '<h2 class="entry-title">', '</h2>' );
					else :
						the_title( '<h2 class="entry-title"><a href="' . esc_url( get_permalink() ) . '">', '</a></h2>' );
					endif;
				?>
			</div>	
			
			<?php hotel_california_blog_meta_content(); ?>
			
			<div class="entry-content">
			<?php the_content( __('Read More','hotel-california') ); ?>
			</div>							
		</div>
</article>