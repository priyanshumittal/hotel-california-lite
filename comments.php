<?php
/**
 * The default template for comments
 */
if ( post_password_required() ) : ?>
	<p class="nopassword"><?php _e( 'This post is password protected. Enter the password to view any comments.', 'hotel-california' ); ?></p>
<?php 
	return;
endif;

// webriti comment part
if( !function_exists('hotel_california_comments') ):
function hotel_california_comments( $comment, $args, $depth ){
	
	$GLOBALS['comment'] = $comment;
	
	//get theme data
	global $comment_data;
	
	//translations
	$leave_reply = $comment_data['translation_reply_to_coment'] ? $comment_data['translation_reply_to_coment'] : __('Reply','hotel-california');
?>
<!--Comment-->
<div <?php comment_class('media comment-box'); ?> id="comment-<?php comment_ID(); ?>">
	<a class="pull-left-comment" href="<?php the_author_meta('user_url'); ?>">
		<?php echo get_avatar( $comment , $size = 70 ); ?>
	</a>
	<div class="media-body">
		<div class="comment-detail">
			<h4 class="comment-detail-title"><?php comment_author(); ?> <span class="comment-date"><?php _e('Posted on ','hotel-california'); ?><?php  echo comment_time('g:i a'); ?><?php echo " - "; echo comment_date('M j, Y');?></span></h4>
			<p><?php comment_text(); ?></p>
			
			<?php edit_comment_link( __( 'Edit', 'hotel-california' ), '<p class="edit-link">', '</p>' ); ?>
					
			<?php if ( $comment->comment_approved == '0' ) : ?>
				<em class="comment-awaiting-moderation"><?php _e( 'Your comment is awaiting moderation.', 'hotel-california' ); ?></em><br/>
			<?php endif; ?>
			
			<div class="reply">
				<?php comment_reply_link(array_merge( $args, array('reply_text' => $leave_reply,'depth' => $depth, 'max_depth' => $args['max_depth'], 'per_page' => $args['per_page']))) ?>
			</div>
			
		</div>	
	</div>
</div>
<!--/Comment-->
<?php
}
endif;
?>

<?php if( have_comments() ): ?>				
<!--Comment Section-->
<article class="comment-section wow fadeInDown animated" data-wow-delay="0.4s">
	<div class="comment-title"><h3><i class="fa fa-comment-o"></i> <?php comments_number ( __('No Comments','hotel-california'), __( '1 Comment','hotel-california'), __('% Comments','hotel-california') ); ?></h3></div>				
	<?php wp_list_comments( array( 'callback' => 'webriti_comments' ) ); ?>			
</article>
<!--/Comment Section-->
<?php endif; ?>

<?php if ( get_comment_pages_count() > 1 && get_option( 'page_comments' ) ) : ?>
<nav id="comment-nav-below">
	<h1 class="assistive-text"><?php _e( 'Comment navigation', 'hotel-california' ); ?></h1>
	<div class="nav-previous"><?php previous_comments_link( __( '&larr; Older Comments', 'hotel-california' ) ); ?></div>
	<div class="nav-next"><?php next_comments_link( __( 'Newer Comments &rarr;', 'hotel-california' ) ); ?></div>
</nav>
<?php endif; ?>

<?php if ( ! comments_open() && get_comments_number() ) : ?>
	<p class="nocomments"><?php _e( 'Comments are closed.' , 'hotel-california' ); ?></p>
<?php endif; ?>


<?php if ('open' == $post->comment_status) :
	if ( get_option('comment_registration') && !$user_ID ): ?>
		<p><?php _e("You must be",'hotel-california'); ?> <a href="<?php echo get_option('siteurl'); ?>/wp-login.php?redirect_to=<?php echo urlencode(get_permalink()); ?>"><?php _e('logged in','hotel-california')?></a> <?php _e("to post a comment",'hotel-california'); ?></p>
	<?php else:
	
	echo '<article class="comment-form-section wow fadeInDown animated" data-wow-delay="0.4s">';
	
	$fields = array(
	'author'=>'<div class="blog-form-group"><input type="text" name="author" id="author" placeholder="'.__('Name:','hotel-california').'" class="blog-form-control"></div>',
	'email'=>'<div class="blog-form-group"><input type="text" name="email" id="email" placeholder="'.__('Email:','hotel-california').'" class="blog-form-control"></div>',
	);
	
	function hotel_california_fields( $fields )
	{
		return $fields;
	}
	add_filter('comment_form_default_fields','hotel_california_fields');
	
	$defaults = array(
		
		'fields'=> apply_filters( 'comment_form_default_fields', $fields ),
		'comment_field'=>'<div class="blog-form-group-textarea"><textarea id="comments" name="comment" placeholder="'.__('Message:','hotel-california').'" class="blog-form-control-textarea" rows="5"></textarea></div>',
		'logged_in_as'=>'<p class="blog-post-info-detail">' . __( "Logged in as ",'hotel-california' ).'<a href="'. admin_url( 'profile.php' ).'">'.$user_identity.'</a>'. '<a href="'. wp_logout_url( get_permalink() ).'" title="'.__('Log out of this Account','hotel-california').'">'.__(" Log out?",'hotel-california').'</a>' . '</p>',
		'id_submit'=> 'blogdetail-btn',
		'label_submit'=>__( 'Send Message','hotel-california'),
		'class_submit'=>'blogdetail-btn',
		'comment_notes_after'=> '',
		'comment_notes_before' => '',
		'title_reply'=> '<div class="comment-title"><h3><i class="fa fa-comment-o"></i>'.__('Leave a Reply', 'hotel-california').'</h3></div>',
		'id_form'=> 'commentform'
		
	);
	ob_start();
	comment_form($defaults);
	echo str_replace('class="comment-form"','class="form-inline"',ob_get_clean());
	
	echo '</article>';
	
	endif;
endif;

if ( ! comments_open() && ! is_page() ) : 
    _e("Comments Are Closed!!!",'hotel-california');
endif; 
?>