<?php
/**
 * The default template for "No Posts Found" message
 */
?>
<article id="post-<?php the_ID(); ?>" <?php post_class( 'post-content-area wow fadeInDown animated' ); ?> data-wow-delay="0.4s">

		<?php if(get_theme_mod('blog_title_position_enable',false) == true){ ?>
		<div class="entry-header">
			<?php
				if ( is_single() ) :
					the_title( '<h2 class="entry-title">', '</h2>' );
				else :
					the_title( '<h2 class="entry-title"><a href="' . esc_url( get_permalink() ) . '">', '</a></h2>' );
				endif;
			?>
		</div>	
		<?php } ?>
		
		<?php if ( is_single() ) {
			echo '<div class="blog-featured-img">';
			the_post_thumbnail( '', array( 'class'=>'img-responsive','alt' => get_the_title() ) );
			echo '</div>';
		}else{
			echo '<div class="blog-featured-img"><a class="post-thumbnail" href="'.get_the_permalink().'">';
			the_post_thumbnail( '', array( 'class'=>'img-responsive','alt' => get_the_title() ) );
			echo '</a></div>';
		} ?>
		
		<div class="post-content">
			
			<?php if(get_theme_mod('blog_title_position_enable',false) == false){ ?>
			<div class="entry-header">
				<?php
					if ( is_single() ) :
						the_title( '<h2 class="entry-title">', '</h2>' );
					else :
						the_title( '<h2 class="entry-title"><a href="' . esc_url( get_permalink() ) . '">', '</a></h2>' );
					endif;
				?>
			</div>	
			<?php } ?>
			
			<?php hotel_california_blog_meta_content(); ?>
			<div class="video-wrapper">
			<?php the_content(__('Read More','hotel-california')); ?>
			</div>
			<?php 
			wp_link_pages( array(
				'before'      => '<div class="page-links"><span class="page-links-title">' . __( 'Pages:', 'hotel-california' ) . '</span>',
				'after'       => '</div>',
				'link_before' => '<span>',
				'link_after'  => '</span>',
			) );
			
			// Edit link
			edit_post_link( __( 'Edit', 'hotel-california' ), '<span class="edit-link">', '</span>' );
			?>							
		</div>
</article>